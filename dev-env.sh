#!/bin/sh
echo Starting to set environment variable

#Service versions
export REPOSITORY_TAG=ubuntuVB:18079 
export DISCOVERY_SERVICE_TAG=0.0.1.6
export API_GATEWAY_SERVER_TAG=0.0.1.5
export MONITORING_SERVICE_TAG=0.0.1.6
export TURBINE_SERVICE_TAG=0.0.1.5
export EMPLOYEE_SERVICE_TAG=0.0.1.7
export DEPARTMENT_SERVICE_TAG=0.0.1.6

#IP configurations
export EUREKA_SERVER_HOST=172.16.72.208
export EUREKA_SERVER_PORT=8761

#Run docker compose
docker-compose up -d
