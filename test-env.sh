#!/bin/sh
echo Starting to set environment variable

#Service versions
export REPOSITORY_TAG=ubuntuVB:18079
export DISCOVERY_SERVICE_TAG=0.0.1
export API_GATEWAY_SERVER_TAG=0.0.1
export MONITORING_SERVICE_TAG=0.0.1
export TURBINE_SERVICE_TAG=0.0.1
export EMPLOYEE_SERVICE_TAG=0.0.1
export DEPARTMENT_SERVICE_TAG=0.0.1

#IP configurations
export EUREKA_SERVER_HOST=192.168.1.98
export EUREKA_SERVER_PORT=8761

#Run docker compose
docker-compose up -d