# Docker-Aggregator

Docker aggregator is compose base service to run all required service together for specified environment

## Installing Composer

These steps are taken from https://docs.docker.com/compose/install/

1. Go to the Compose repository release page on GitHub.

	https://github.com/docker/compose/releases
	
2. Use following command to install composer

	$ curl -L https://github.com/docker/compose/releases/download/1.11.2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
	$ chmod +x /usr/local/bin/docker-compose
	
3. Test the installation.

	$ docker-compose --version
	docker-compose version: 1.11.2
	

Note: If you get a �Permission denied� error, your /usr/local/bin directory probably isn�t writable and you�ll need to install Compose as the superuser. Run sudo -i, then the two commands above, then exit

## Running Dokcer-Aggregator

This service is executed through shell script provide in same folder. To run as dev environment use script 'dev-env.sh'. Provided script set variable for all service within docker aggregator and run all the container configured in docker-composer.yml  

Got docker-aggregator folder and run below given command sequentially. 

	$ chmod a+rx dev-env.sh
	$ . ./dev-env.sh

## Useful commands for docker composer

To run all services

	$ docker-compose up
	use -d for "detached mode"
	
To check currently running processes
	
	$ docker-compose ps
	
To stop all services

	$ docker-compose stop
	
To get logs 

	$ docker-compose logs
	
	